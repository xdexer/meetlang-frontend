import { Navigate } from 'react-router-dom';
import AuthService from '../services/auth/auth-service';


interface Props {
    route: JSX.Element
}

const RouteGuard = ({ route }: Props): JSX.Element => {
    const user = AuthService.getUser()

    if (user && user.access)
        return route;
    return <Navigate to='/login' />
}

export default RouteGuard;