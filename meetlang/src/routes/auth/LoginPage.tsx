import styled from 'styled-components'
import { Typography, TextField, Button, Divider, Alert } from '@mui/material'
import { useNavigate } from 'react-router-dom'
import AuthService from '../../services/auth/auth-service'
import { useState, useEffect } from 'react'
import { LoginData } from '../../services/auth/types'
import color from '../../components/consts/colors'

const Wrapper = styled.div`
    background: ${color.gradient};
    min-height: 100vh;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    font-size: 1em;
    color: black;
`
const Container = styled.div`
    background-color: white;
    display: flex;
    flex-direction: column;
    align-items: center;
    outline: 1px solid darkgrey;
    @media (max-width: 540px) {
        width: 100%
    }
`
const TitleContainer = styled.div`
    display: flex;
    justify-content: center;
    padding-top: 2em;
    padding-bottom: 2em;
`
const FormContainer = styled.form`
    display: flex;
    flex-direction: column;
    justify-content: center;
    margin-left: 20px;
    margin-right: 20px;
    margin-bottom: 20px;
    width: 25em;
    row-gap: 1em;
    @media (max-width: 540px) {
        width: 100%;
    }
`

const LoginPage = () => { 
    const [form, setForm] = useState<LoginData>({
        email: '',
        password: '',
    })
    const [formError, setError] = useState<Array<string>>([])
    const navigate = useNavigate()

    useEffect(() => {
        if (AuthService.isAuthenticated()) {
            navigate('/')
        }
    }, [navigate])


    const onUpdateField = (e: React.ChangeEvent<HTMLInputElement>): void => {
        setForm({
            ...form,
            [e.target.name]: e.target.value
        })
    }

    const onSubmit = async (e: React.FormEvent): Promise<void> => {
        e.preventDefault()
        try {
            await AuthService
                .login(form)
                .then(
                    (response) => {
                        console.log(response);
                        navigate('/')
                    },
                    (error) => {
                        if (error.response.data) {
                            const data = error.response.data
                            const errors = []
                            for (const msg in data) errors.push(data[msg])
                            setError(errors)
                        }
                    }
                )
        }
        catch (error) {
            console.log(error)
        }
    }

    const handleSignUp = (): void => {
        navigate('/signup')
    }

    

    return (
        <Wrapper>
            <Container>
                <TitleContainer>
                    <Typography variant='h3'>Meetlang</Typography>
                </TitleContainer>
                <FormContainer onSubmit={onSubmit}>
                    <TextField name='email' label='Email' onChange={onUpdateField} required/>
                    <TextField type='password' name='password' label='Password' onChange={onUpdateField} required/>
                    <Button type='submit' variant='contained'>
                        Log in
                    </Button>
                    <Divider variant='middle' flexItem></Divider>
                    <Button variant='contained' onClick={handleSignUp}>
                        Sign Up!
                    </Button>
                    {formError.length > 0 && <Alert severity='error'>{formError.map(el => <Typography>{el}</Typography>)}</Alert>}
                </FormContainer>
            </Container>
        </Wrapper>
    )
}

export default LoginPage