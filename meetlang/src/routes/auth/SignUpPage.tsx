import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import styled from 'styled-components'
import { Typography, TextField, Button, Divider, Alert } from '@mui/material'
import AuthService from '../../services/auth/auth-service'
import { SignUpData } from '../../services/auth/types'
import color from '../../components/consts/colors'

const Wrapper = styled.div`
    background: ${color.gradient};
    min-height: 100vh;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    font-size: 1em;
    color: black;
`
const Container = styled.div`
    background-color: white;
    display: flex;
    flex-direction: column;
    align-items: center;
    outline: 1px solid darkgrey;
    @media (max-width: 540px) {
        width: 100%
    }
`
const TitleContainer = styled.div`
    display: flex;
    justify-content: center;
    padding-top: 2em;
    padding-bottom: 2em;
`
const FormContainer = styled.form`
    display: flex;
    flex-direction: column;
    justify-content: center;
    margin-left: 20px;
    margin-right: 20px;
    margin-bottom: 20px;
    width: 25em;
    row-gap: 1em;
    @media (max-width: 540px) {
        width: 100%;
    }
`

const SignUpPage = () => {
    const navigate = useNavigate()
    const [form, setForm] = useState<SignUpData>({
        email: '',
        password: '',
        confirm_password: ''
    })
    const [formError, setError] = useState<Array<string>>([])

    const onUpdateField = (e: React.ChangeEvent<HTMLInputElement>): void => {
        setForm({
            ...form,
            [e.target.name]: e.target.value
        })
    }


    const onSubmit = async (e: React.FormEvent): Promise<void> => {
        e.preventDefault()
        try {
            await AuthService
                .signup(form)
                .then(
                    (response) => {
                        localStorage.setItem('firstLogin', 'true')
                        navigate('/login');
                    },
                    (error) => {
                        if (error.response.data) {
                            const data = error.response.data
                            const errors = []
                            for (const msg in data) errors.push(data[msg])
                            setError(errors)
                        }
                    }
                )
        }
        catch (error) {
            console.log(error)
        }
    }

    return (
        <Wrapper>
            <Container>
                <TitleContainer>
                    <Typography variant='h3'>Sign Up</Typography>
                </TitleContainer>
                <FormContainer onSubmit={onSubmit}>
                    <TextField name='email' label='Email' value={form.email} onChange={onUpdateField} required/>
                    <TextField type='password' name='password' label='Password' value={form.password} onChange={onUpdateField} required/>
                    <TextField type='password' name='confirm_password' label='Confirm Password' value={form.confirm_password} onChange={onUpdateField} required/>
                    <Divider variant='middle' flexItem></Divider>
                    <Button type='submit' variant='contained'>Sign up</Button>
                    <Button variant='contained' onClick={() => navigate('/login')}>Back</Button>
                    {formError.length > 0 && <Alert severity='error'>{formError.map(el => <Typography>{el}</Typography>)}</Alert>}
                </FormContainer>
            </Container>
        </Wrapper>
    )
}

export default SignUpPage