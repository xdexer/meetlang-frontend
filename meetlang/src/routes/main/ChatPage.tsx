import Chat from "../../components/chat/Chat"
import { useParams } from 'react-router-dom'


const ChatPage = () => {

    const { chatName } = useParams();

    return (
        <Chat chatName={chatName}/>
    );
}

export default ChatPage