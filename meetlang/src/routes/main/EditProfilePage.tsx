import Card from '@mui/material/Card';
import { TextField, Select, MenuItem, FormControl, InputLabel, Button } from '@mui/material';
import Typography from '@mui/material/Typography';
import { ReadMoreRounded, Send } from '@mui/icons-material';
import Divider from '@mui/material/Divider';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import { useEffect, useState, Dispatch, SetStateAction, ChangeEvent } from 'react';
import MultipleSelectChip from '../../components/edit_profile/MultiselectChip';
import Avatar from '@mui/material/Avatar';
import { Fab } from '@mui/material';
import EditProfileService from '../../services/edit_profile/edit-profile-service';
import { Tag, Profile, Language, LanguageResponse, ProfileResponse } from '../../interfaces/edit-profile';
import { useNavigate, useOutletContext } from 'react-router-dom';
import CentrifugoService from '../../services/centrifuge/centrifugo-service';
import { stringAvatar } from '../../utils/utils';
import { Add } from '@mui/icons-material';


interface ContextType {
    alertOpen: Dispatch<SetStateAction<boolean>>
    centifugo: CentrifugoService
}

const EditProfilePage = () => {
    const [form, setForm] = useState<Profile>({
        name: '',
        gender: 0,
        age: 18,
        description: '',
        nativeLanguage: '',
        desiredLanguage: '',
        avatar: null,
        tags: []
    })
    const [languages, setLanguages] = useState<Array<Language>>([])
    const [tags, setTags] = useState<Array<Tag>>([])
    const { alertOpen } = useOutletContext<ContextType>()
    const [initTags, setInitTags] = useState<Array<Tag>>([])
    const [isDisabled, setIsDisabled] = useState<boolean>(true)
    const [image, setImage] = useState<string>('')

    const navigate = useNavigate()
    
    useEffect(() => {
        EditProfileService.getUserDefaults()
        .then(response => {
            const data: ProfileResponse = response.data
                setInitTags(data.tags)
                setForm({
                    name: data.first_name,
                    gender: data.sex ? 1 : 0,
                    age: data.age,
                    description: data.user_description,
                    nativeLanguage: `${data.native_language.name}`,
                    desiredLanguage: `${data.language_to_learn.name}`,
                    tags: data.tags,
                    avatar: null
                })
                if (data.avatar) {
                    setImage(data.avatar)
                }
                setIsDisabled(false)
            })
        EditProfileService.getLanguages()
            .then(response => {
                const data: Array<LanguageResponse> = response.data
                setLanguages(data.map((lang) => {
                    return {
                        id: lang.id,
                        name: lang.name
                    }
                }))
            })
        EditProfileService.getTags()
            .then(response => {
                const data: Array<Tag> = response.data
                setTags(data.map((tag) => {
                    return {
                        id: tag.id,    
                        name: tag.name
                    }
                }))
            })
    }, [])
        
    const onUpdateField = (e: any): void => {
        const newData = {
            ...form,
            [e.target.name]: e.target.value
        }
        setForm(newData)
        setIsDisabled(validateFields(newData))
    }

    const onAvatarUpdate = (avatar: File): void => {
        const newData = {
            ...form,
            avatar: avatar
        }
        console.log(newData)
        setForm(newData)
    }


    const onTagsChange = (tags: Array<Tag>) => {
        const newData = {
            ...form,
            tags: tags
        }
        setForm(newData)
        console.log('TAGS:', tags, validateFields(newData))
        setIsDisabled(validateFields(newData))
    }

    const getAgeRange = (): number[] => {
        return Array.from(
            { 
                length: 99
            },
            (value, index) => index + 1
        )
    }

    const handleSubmit = () => {
        const data = {
            ...form,
            nativeLanguage: languages.filter(el => el.name === form.nativeLanguage)[0].id,
            desiredLanguage: languages.filter(el => el.name === form.desiredLanguage)[0].id
        }
        EditProfileService.postData(data).then(response => {
            if (response)
                EditProfileService.uploadImageRequest(data)
            console.log(alertOpen)
            alertOpen(true)
            navigate('/match')
        })
    }
    
    const validateFields = (data: Profile): boolean => {
        return !(
            Boolean(data.name) && Boolean(data.description) && 
            Boolean(data.nativeLanguage) && Boolean(data.desiredLanguage) && 
            Boolean(data.tags.length)
        )
    
    }

    const handleFileUpload = (e: ChangeEvent<HTMLInputElement>) => {
        if (!e.target.files)
            return

        const file = e.target.files[0]
        onAvatarUpdate(file)
        setImage(URL.createObjectURL(file))
    }

    return (
        <Card sx={{ minWidth: 300, width: 500, height: '100vh', display: 'flex', flexDirection: 'column', overflowY: 'auto'}}>
            <Box sx={{ height: '64px', minHeight: '64px'}}></Box>
            <Container sx={{ display: 'flex', flexDirection:'column', alignItems: 'center', mt: '32px'}}>
                { 
                    image ? 
                        <Avatar src={image} sx={{ height: '124px', width: '124px'}}/> : 
                        <Avatar {...stringAvatar(form.name ? form.name : 'Name', { height: '124px', width: '124px'})} /> 
                }
                <label htmlFor="upload-photo">
                    <input
                        style={{ display: 'none' }}
                        id="upload-photo"
                        name="upload-photo"
                        type="file"
                        accept="image/jpeg, image/png, image/jpg"
                        onChange={handleFileUpload}
                    />
                    <Fab
                        color="secondary"
                        size="small"
                        component="span"
                        aria-label="add"
                        variant="extended"
                        sx= {{ marginTop: '12px', marginBottom: '12px'}}
                    >
                        <Add /> Upload photo
                    </Fab>
                </label>
            </Container>
            <Divider />
            <Box sx={{ m: '15px' }}>
                <Typography variant="body1">
                    Your name:
                </Typography>
                <TextField name='name' inputProps={{ maxLength: 30 }} value={form.name} onChange={onUpdateField} sx={{ mt: '15px'}} size='small' required={true}/>
            </Box>
            <Divider />
            <Box sx={{ m: '15px' }}>
                <Typography variant="body1">
                    Age:
                </Typography>
                <Select
                    sx={{ mt: '15px', width: '64px'}}
                    value={form.age}
                    onChange={onUpdateField}
                    name='age'
                    required={true}
                    MenuProps={
                        {
                            PaperProps: {
                                style: {
                                maxHeight: 256
                                }
                            }
                        }
                    }   
                >
                    { getAgeRange().map(el => <MenuItem value={el}>{el}</MenuItem>) }
                </Select>
            </Box>
            <Divider />
            <Box sx={{ m: '15px' }}>
                <Typography variant="body1">
                    Gender:
                </Typography>
                <Select
                    sx={{ mt: '15px'}}
                    value={form.gender}
                    onChange={onUpdateField}
                    name='gender'
                    required={true}
                    >
                    <MenuItem value={1}>Man</MenuItem>
                    <MenuItem value={0}>Woman</MenuItem>
                </Select>
            </Box>
            <Divider />
            <Box sx={{ m: '15px' }}>
                <Typography variant="body1">
                    Languages:
                </Typography>
                <FormControl sx={{ mt: '15px', mr: '24px', width: '200px'}}>
                    <InputLabel id="native-lang">Native</InputLabel>
                    <Select
                        labelId='native-lang'
                        label='Native'
                        value={form.nativeLanguage}
                        onChange={onUpdateField}
                        name='nativeLanguage'
                        required={true}
                    >
                        {
                            languages
                                .filter((el) => el.name !== form.desiredLanguage)
                                .map((el) => <MenuItem value={el.name}>{el.name}</MenuItem>)
                        }
                    </Select>
                </FormControl>
                <FormControl sx={{ mt: '15px', width: '200px'}}>
                    <InputLabel id="desired-lang">Desired</InputLabel>
                    <Select
                        labelId='desired-lang'
                        label='Desired'
                        value={form.desiredLanguage}
                        onChange={onUpdateField}
                        name='desiredLanguage'
                        required={true}
                    >   
                        {
                            languages
                                .filter((el) => el.name !== form.nativeLanguage)
                                .map((el) => <MenuItem value={el.name}>{el.name}</MenuItem>)
                        }
                    </Select>
                </FormControl>
            </Box>
            <Divider />
            <Box sx={{ m: '15px' }}>
                <Typography variant="body1">
                    Description:
                </Typography>
                <FormControl fullWidth>
                    <TextField name='description' inputProps={{ maxLength: 1000 }} value={form.description} onChange={onUpdateField} size='small' multiline rows={4} required/>
                </FormControl>
            </Box>
            <Divider />
            <Box sx={{ m: '15px' }}>
                <Typography variant="body1">
                    Tags:
                </Typography>
                <MultipleSelectChip onChange={onTagsChange} data={tags} initialData={initTags}/>
            </Box>
            <Divider />
            <Box sx={{ m: '15px', display: 'flex', justifyContent: 'flex-end'}}>
                <Button variant="contained" disabled={isDisabled} endIcon={<Send />} onClick={handleSubmit}>
                    Submit
                </Button>
            </Box>
        </Card>
    );
}

export default EditProfilePage