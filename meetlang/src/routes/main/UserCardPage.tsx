import { useState, useEffect } from 'react';
import UserCard from "../../components/user-card/UserCard";
import MatchService from '../../services/match/match-service';
import CircularProgress from '@mui/material/CircularProgress';
import { ProfileResponse } from '../../interfaces/edit-profile';
import { Typography, Box, Button } from '@mui/material';


const UserCardPage = () => {
    const [userCards, setUserCards] = useState<ProfileResponse | null >(null)
    const [noMatches, setNoMatches] = useState<boolean>(false)
    const matchService = new MatchService()

    useEffect(() => {
        loadUserCard()
    }, [])

    const loadUserCard = () => {
        matchService.getUserCardsInfo()
        .then(response => {
            console.log(response.status)
            if (response.status == 204) {
                setUserCards(null)
                setNoMatches(true)
            }
            if (response.data) {
                setNoMatches(false)
                setUserCards(response.data)
            }
        })
    }

    const handleMatchAccept = (): void => {
        if (!userCards) 
            return
        matchService.acceptMatch(userCards.user)
        loadUserCard()
    }

    const handleMatchRefuse = (): void => {
        if (!userCards)
            return
        matchService.refuseMatch(userCards.user)
        loadUserCard()
    }

    if (noMatches) {
        return (
            <Box sx={{ display: 'flex', maxHeight: '45px', minHeight: '45px', width: '100%', alignSelf: 'center', flexDirection: 'column'}} >
                <Typography sx={{ alignSelf: 'center' }}>
                    No meetlangs for now. Try again later!
                </Typography> 
                <Button onClick={loadUserCard}>Refresh?</Button>
            </Box>
        )
    }

    return (
        userCards ? <UserCard user={userCards} handleAccept={handleMatchAccept} handleRefuse={handleMatchRefuse} /> 
            : <CircularProgress sx={{ alignSelf: 'center' }} /> 
    );
}

export default UserCardPage