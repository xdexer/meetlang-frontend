import { Container, Snackbar, Alert, Box } from '@mui/material'
import CssBaseline from '@mui/material/CssBaseline';
import { useEffect, useState } from 'react';
import { Outlet, useLocation, useNavigate } from 'react-router-dom';
import DrawerController from '../../components/drawer/DrawerController';
import Header from '../../components/header/Header';
import jwtInterceptor from '../../utils/interceptors/jwtInterceptor';
import config from '../../production.config.json'
import CentrifugoService from '../../services/centrifuge/centrifugo-service';
import { Centrifuge } from 'centrifuge';

let centrifugo: CentrifugoService | null = null

const MainPage = () => {
    const [mobileOpen, setMobileOpen] = useState<boolean>(false);
    const [alertOpen, setAlertOpen] = useState<boolean>(false)
    const location = useLocation()
    const navigate = useNavigate()

    useEffect(() => {
        setUserId()
        centrifugo = new CentrifugoService(new Centrifuge(`ws://${config.MEETLANG_DOMAIN}${config.CENTRIFUGO_URL}`, 
        {
            debug: true,
        }))

        window.addEventListener('beforeunload', handleTabClose)
        return () => {
            console.log('MAIN PAGE UNMOUNT')
            window.removeEventListener('beforeunload', handleTabClose);
            centrifugo?.disconnectClient()
            centrifugo = null
        }
    }, [])


    const checkIfFirstLogin = () => {
        if (location.pathname === '/') {
            if (localStorage.getItem('firstLogin') === 'true')
                navigate('/edit_profile')
            else {
                navigate('/match')
                setUserStatus(true)
            }
        }
    }

    const setUserId = () => {
        jwtInterceptor.get(
            `${config.API_URL}/user/user_management/get_current_user_id/`
        ).then(response => {
            localStorage.setItem('userId', response.data.user)
            checkIfFirstLogin()
        })
    }
    
    const handleTabClose = (event: any) => {
        event.preventDefault();
  
        console.log('beforeunload event triggered');
        setUserStatus(false)
        return ''
      };

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };


    const handleDrawerOff = () => {
        setMobileOpen(false);
    };

    const setUserStatus = (isOnline: boolean) => {
        console.log('USER STATUS SET: ', isOnline)
        jwtInterceptor.patch(
            `${config.API_URL}/user/user_management/${localStorage.getItem('userId')}/`,
            {
                is_active: isOnline
            }
        ).then(response => {
            console.log('USER STATUS SET RESPONSE: ', response)
        })
    }

    return (
        <Box sx={{ display: 'flex', height: '100%', width: 'auto', bgcolor: '#fafafa' }}>
            <CssBaseline />
            <Header handleDrawerToggle={handleDrawerToggle} />
            <DrawerController isOpen={mobileOpen} handleDrawerToggle={handleDrawerToggle} handleDrawerOff={handleDrawerOff}/>
            <Container
                sx={{ display: 'flex', justifyContent: 'center', flexGrow: 1, width: '100%', height: '100vh' }}
            >
                <Outlet context={{ alertOpen: setAlertOpen, centifugo: centrifugo}}/>
            </Container>
            <Snackbar anchorOrigin={{vertical: 'bottom', horizontal: 'right'}} open={alertOpen} autoHideDuration={6000} onClose={() => setAlertOpen(false)}>
                <Alert severity="success" sx={{ width: '100%' }} onClose={() => setAlertOpen(false)}>
                    Profile updated!
                </Alert>
            </Snackbar>
        </Box>
    );
}

export default MainPage