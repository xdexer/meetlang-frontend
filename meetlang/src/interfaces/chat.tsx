interface MessageData {
    message: string
    timestamp: string
    user: number
}

interface MessageResponse {
    message: string
    timestamp: string
    user: number
    id: number,
    chat: number
}

interface ChatSummary {
    other_user_id: number,
    other_user_name: string,
    last_message: string,
    chat_id: string,
    native_flag: string,
    learning_flag: string,
    avatar: string | null
    is_online: boolean
}

export type { MessageData, MessageResponse, ChatSummary }