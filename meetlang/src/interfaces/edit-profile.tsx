interface Tag {
    id: number,
    name: string
}

interface Profile {
    name: string,
    gender: number,
    age: number,
    description: string
    nativeLanguage: string
    desiredLanguage: string
    avatar: Blob | null
    tags: Array<Tag>
}

interface Language {
    id: number,
    name: string
}

interface LanguageResponse {
    id: number,
    name: string,
    code: string,
    common: boolean,
    flag: string
}

interface ProfileResponse {
    id: number,
    first_name: string,
    is_active: boolean,
    user_description: string,
    age: number,
    sex: boolean,
    native_language: LanguageResponse,
    language_to_learn: LanguageResponse,
    user: number,
    tags: Array<Tag>,
    avatar: string | null
}

interface ProfileRequest {
    name: string,
    gender: number,
    age: number,
    description: string
    nativeLanguage: number
    desiredLanguage: number
    tags: Array<Tag>
    avatar: Blob | null
}

export type { Tag, Profile, Language, LanguageResponse, ProfileResponse, ProfileRequest }