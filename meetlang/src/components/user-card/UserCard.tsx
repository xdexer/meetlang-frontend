import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import Container from '@mui/material/Container';
import CardContent from '@mui/material/CardContent';
import { Chip } from '@mui/material';
import CardActions from '@mui/material/CardActions';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import CheckIcon from '@mui/icons-material/Check';
import ClearIcon from '@mui/icons-material/Clear';
import Divider from '@mui/material/Divider';
import Box from '@mui/material/Box';
import { stringAvatar, stringToColor } from '../../utils/utils';
import { ProfileResponse } from '../../interfaces/edit-profile';

interface Props {
    user: ProfileResponse
    handleAccept: () => void,
    handleRefuse: () => void
}


const UserCard = (props: Props) => {
    const {user, handleAccept, handleRefuse} = props

    const onAcceptClick = () => {
        handleAccept()
    }

    const onRefuseClick = () => {
        handleRefuse()
    }

    return (
        <Card sx={{ minWidth: 300, width: 500, height: '100vh', display: 'flex', flexDirection: 'column', overflowY: 'auto'}}>
            <Box sx={{ height: '64px', minHeight: '64px' }}></Box>
            <Container sx={{ display: 'flex', flexDirection:'column', alignItems: 'center', mt: '32px'}}>
                {
                    user.avatar ?
                    <Avatar src={user.avatar} sx={{ height: '124px', width: '124px'}}/> : 
                    <Avatar {...stringAvatar(user.first_name, { height: '124px', width: '124px'})} /> 
                }
                <CardHeader
                    title={`${user.first_name}, ${user.age}`}
                />
            </Container>
            <Divider />
            <Box sx={{ m: '15px' }}>
                <Typography variant="body1">
                    Sex: {user.sex ? 'Man' : 'Woman'}
                </Typography>
            </Box>
            <Divider />
            <Box sx={{ m: '15px', display: 'flex' }}>
                <Typography variant="body1" sx={{ flexGrow: 1 }}>
                    Knows: {user.native_language.name} {user.native_language.flag}
                </Typography>
                <Typography variant="body1" sx={{ flexGrow: 1 }}>
                    Learns: {user.language_to_learn.name} {user.language_to_learn.flag}
                </Typography>
            </Box>
            <Divider />
            <Box sx={{ m: '15px' }}>
                <Typography variant="body1">
                    Interests:
                </Typography>
                {user.tags.map((el, index) => <Chip key={index}label={el.name} sx={{ bgcolor: stringToColor(el.name), color: 'white', fontWeight: 'bold', marginRight: '6px' }}/>)}
            </Box>
            <Divider />
            <CardContent sx={{ flexGrow: 1 }}>
                <Typography variant="body1">
                    Description:
                </Typography>
                <Typography variant="body2" color="text.secondary">
                    {user.user_description}
                </Typography>
            </CardContent>
            <Box>
                <Divider />
                <CardActions disableSpacing>
                    <IconButton aria-label="Refuse" sx={{ ml: '50px' }} onClick={onRefuseClick}>
                        <ClearIcon color='error' sx={{ color: 'error', height: '52px', width: '52px'}} />
                    </IconButton>
                    <IconButton aria-label="Accept" sx={{ ml: 'auto', mr: '50px' }} onClick={onAcceptClick}>
                        <CheckIcon color='success' sx={{ height: '52px', width: '52px'}}/>
                    </IconButton>
                </CardActions>
            </Box>
        </Card>
    );
}

export default UserCard