import { useEffect, useState } from 'react';
import { Theme, useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import Chip from '@mui/material/Chip';
import { Tag } from '../../interfaces/edit-profile';
import { stringToColor } from '../../utils/utils';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};


function getStyles(name: string, personName: readonly string[], theme: Theme) {
  return {
    fontWeight:
      personName.indexOf(name) === -1
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightMedium,
  };
}

interface Props {
  onChange: (tags: Array<Tag>) => void
  data: Array<Tag>
  initialData: Array<Tag>
}

export default function MultipleSelectChip(props: Props) {
  const theme = useTheme();
  const [tagsName, setTagsName] = useState<Array<string>>([]);

  useEffect(() => {
    console.log('Initial DATA: ', props.initialData)
    setTagsName(props.initialData.map(el => `${el.name},${el.id}`))
  }, [props.initialData])

  const handleChange = (event: SelectChangeEvent<typeof tagsName>) => {
    const {
      target: { value },
    } = event;

    if (Array.isArray(value)) {
      const tagsArray = value.map((el) => {
        const tag = el.split(',')
        return {
          id: Number(tag[1]),
          name: tag[0]
        }
      })
      props.onChange(tagsArray)
    }
    setTagsName(
      // On autofill we get a stringified value.
      typeof value === 'string' ? value.split(',') : value,
    );
  };

  return (
    <div>
      <FormControl sx={{ m: 1, width: 300 }}>
        <Select
          id="tag-selector"
          multiple
          value={tagsName}
          onChange={handleChange}
          renderValue={(selected) => (
            <Box sx={{ display: 'flex', flexWrap: 'wrap', gap: 0.5 }}>
              {
                selected.map((value) => {
                  const values = value.split(',')
                  return <Chip key={values[1]} label={values[0]} sx={{ bgcolor: stringToColor(values[0]), color: 'white', fontWeight: 'bold', marginRight: '6px' }}/>
                })
              }
            </Box>
          )}
          MenuProps={MenuProps}
        >
          {props.data.map((tag) => (
            <MenuItem
              key={tag.id}
              value={`${tag.name},${tag.id}`}
              style={getStyles(tag.name, tagsName, theme)}
            >
              {tag.name}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  );
}