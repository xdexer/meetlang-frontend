import { useEffect, useState } from 'react';
import AppBar from '@mui/material/AppBar';
import IconButton from '@mui/material/IconButton';
import { ManageAccounts, Groups, Logout } from '@mui/icons-material';
import MenuIcon from '@mui/icons-material/Menu';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import AuthService from '../../services/auth/auth-service'
import { CONSTANTS } from '../../utils/consts';
import { useNavigate, useLocation } from 'react-router-dom'

interface Props {
    handleDrawerToggle: () => void
}

const Header = (props: Props) => {
    const [isFirstLogin, setFirstLogin] = useState<boolean>(() => {
        return (localStorage.getItem('firstLogin') === 'true')
    })
    const { handleDrawerToggle } = props
    const navigate = useNavigate()
    const location = useLocation()

    useEffect(() => {
        setFirstLogin(localStorage.getItem('firstLogin') === 'true')
    }, [location])

    const handleLogout = () => {
        console.log('HANDLE LOGOUT')
        AuthService.logout()
            .then(response => {
                if (response)
                    navigate('/login')
            })
        console.log('HANDLE NAVIGATE')
    }

    return (
        <AppBar
            position="fixed"
            sx={{
                display: 'flex',
                width: { sm: `calc(100% - ${CONSTANTS.drawerWidth}px)` },
                ml: { sm: `${CONSTANTS.drawerWidth}px` },
            }}
        >
            <Toolbar>
                <IconButton
                    color="inherit"
                    aria-label="open drawer"
                    edge="start"
                    onClick={handleDrawerToggle}
                    sx={{ mr: 2, display: { sm: 'none' } }}
                >
                    <MenuIcon />
                </IconButton>
                <Typography variant="h6" noWrap component="div">
                    Meetlang
                </Typography>
                <IconButton disabled={isFirstLogin} sx={{ ml: '16px' }} onClick={() => navigate('match/')} aria-label="Meet people">
                    <Groups style={{color: 'white'}}/>
                </IconButton>
                <IconButton sx={{ ml: '16px' }} onClick={() => navigate('edit_profile/')} aria-label="Edit profile">
                    <ManageAccounts style={{color: 'white'}}/>
                </IconButton>
                <IconButton sx={{ ml: 'auto' }} onClick={handleLogout} aria-label="Logout">
                    <Logout style={{color: 'white'}}/>
                </IconButton>
            </Toolbar>
        </AppBar>
    )
}

export default Header