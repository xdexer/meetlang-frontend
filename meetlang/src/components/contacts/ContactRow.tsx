import Divider from '@mui/material/Divider';

import ListItem from '@mui/material/ListItem'
import ListItemButton from '@mui/material/ListItemButton'
import ListItemText from '@mui/material/ListItemText'
import { Avatar } from '@mui/material'
import StyledBadge from './StyledBadge'
import { stringAvatar } from '../../utils/utils'
import { useNavigate } from 'react-router-dom';
import { ChatSummary } from '../../interfaces/chat';


interface Props {
    chatData: ChatSummary
    handleDrawer: () => void
}

const ContactRow = ({ chatData, handleDrawer }: Props) => {

    const navigate = useNavigate()

    const handleClick = () => {
        handleDrawer()
        navigate(`chat/${chatData.chat_id}`)
    }
    
    return (
        <>
            <ListItem key={chatData.chat_id} alignItems="flex-start" disablePadding>
                <ListItemButton onClick={handleClick}>
                    <StyledBadge
                        overlap="circular"
                        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
                        variant="dot"
                        isOnline={chatData.is_online}
                    >
                        {
                            chatData.avatar ?
                            <Avatar src={chatData.avatar} /> : 
                            <Avatar {...stringAvatar(chatData.other_user_name)} /> 
                        }
                    </StyledBadge>
                    <ListItemText
                        primary={chatData.other_user_name}
                        secondary={chatData.last_message}
                        secondaryTypographyProps={{noWrap: true, sx: {textOverflow: 'ellipsis', overflow: 'hidden'}}}
                        sx={{paddingLeft: '10px', textOverflow: 'ellipsis', overflow: 'hidden'}}
                    />
                    <ListItemText
                        primary={chatData.native_flag}
                        secondary={chatData.learning_flag}
                        secondaryTypographyProps={ {color: 'black', fontSize: '1rem'}}
                        sx={{textAlign: 'right', minWidth: '20px'}}
                    />
                </ListItemButton>
            </ListItem>
            <Divider />
        </>
    )
}

export default ContactRow