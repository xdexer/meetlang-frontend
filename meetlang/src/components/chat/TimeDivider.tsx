import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { formatTimestamp } from '../../utils/utils';

interface Props {
    timestamp: string
}

const TimeDivider = (props: Props) => {
    const {timestamp} = props

    return (
        <Box sx={{ bgcolor: 'white', width: '100%', display: 'flex', justifyContent: 'center'}}>
            <Typography sx={{ color: 'black', fontWeight: '700', fontSize: '12px'}}>
            {formatTimestamp(timestamp)}
            </Typography>
        </Box>
    );
}

export default TimeDivider