import Message from './Message';
import { Container, Button, TextField } from '@mui/material';
import { MessageData } from '../../interfaces/chat';
import { memo, FunctionComponent } from 'react'
import TimeDivider from './TimeDivider';
import { ONE_HOUR } from './../../utils/consts';

interface Props {
    messages: Array<MessageData>
    userId: number | null
    onTopHandler: () => void
}

const MessageContainer = memo<Props>(function MessageContainer({messages, userId, onTopHandler}: Props) {

    const handleScroll = (event: any) => {
        const { scrollHeight, scrollTop, clientHeight } = event.target
        const scroll = scrollHeight + scrollTop - clientHeight

        if (scroll <= 100)
            onTopHandler()
    }

    const calculateHourDifference = (currTime: string, otherTime: string): number => {
        const currentHours = new Date(currTime).getTime()
        const previousHours = new Date(otherTime).getTime()
        const difference_ms = currentHours - previousHours
        return difference_ms/ONE_HOUR
    }

    return (
        <Container onScroll={handleScroll} sx={{ flexGrow: 1, display: 'flex', flexDirection: 'column-reverse', overflowY: 'auto'}}>
            {
                messages.map((el, index) => {
                    if (messages[index+1] == undefined) {
                        return <>
                            <Message key={index} text={el.message} timestamp={el.timestamp} isOwn={el.user == userId}/>
                            <TimeDivider timestamp={el.timestamp}/>
                        </>
                    }

                    const difference_hours = calculateHourDifference(el.timestamp, messages[index+1].timestamp)
                    if (difference_hours < 1)
                        return <Message key={index} text={el.message} timestamp={el.timestamp} isOwn={el.user == userId}/>
                    return (
                        <>
                            <Message key={index} text={el.message} timestamp={el.timestamp} isOwn={el.user == userId}/>
                            <TimeDivider timestamp={el.timestamp}/>
                        </>
                    )
                })
            }
        </Container>
    )
})

export default MessageContainer