import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { Tooltip } from '@mui/material';
import { formatTimestamp } from '../../utils/utils';

interface Props {
    text: string
    timestamp: string
    isOwn: boolean
}

const Message = (props: Props) => {
    const {text, timestamp, isOwn} = props
    const alinegment = isOwn ? 'flex-end' : 'flex-start';
    const color = isOwn ? '#3498eb' : '#00a608'

    return (
        <Tooltip title={formatTimestamp(timestamp)}>
            <Box sx={{m: '8px', p: '12px', borderRadius: '8px', bgcolor: color, width: 'fit-content', maxWidth: '60%', alignSelf: alinegment}}>
                <Typography sx={{ wordBreak: 'break-word', color: 'white'}}>
                {text}
                </Typography>
            </Box>
        </Tooltip>
    );
}

export default Message