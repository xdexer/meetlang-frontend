import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';

interface Props {
    handleConfirm: () => void,
    handleClose: () => void
    open: boolean
}

const DeleteContactDialog = ({handleConfirm, handleClose, open}: Props) => {

    return (
        <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">
            {"Do you want to reject conversation?"}
            </DialogTitle>
            <DialogContent>
            <DialogContentText id="alert-dialog-description">
                This action will remove the converation history and remove user from contact list.
            </DialogContentText>
            </DialogContent>
            <DialogActions>
            <Button onClick={handleClose}>No</Button>
            <Button onClick={handleConfirm} autoFocus>
                Yes
            </Button>
            </DialogActions>
        </Dialog>
    )
}

export default DeleteContactDialog