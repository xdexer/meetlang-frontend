import { useEffect, useState, useRef, useCallback, Dispatch, SetStateAction }from 'react';
import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import SendIcon from '@mui/icons-material/Send';
import { Container, Button, TextField } from '@mui/material';
import FormControl from '@mui/material/FormControl';
import CentrifugoService from '../../services/centrifuge/centrifugo-service';

import { MessageResponse, MessageData } from '../../interfaces/chat';
import MessageContainer from './MessageContainer';
import { useOutletContext, useNavigate } from 'react-router-dom';
import DeleteContactDialog from './DeleteContactDialog';
import ClearIcon from '@mui/icons-material/Clear';

interface Props {
    chatName: string | undefined
}

interface ContextType {
    alertOpen: Dispatch<SetStateAction<boolean>>
    centifugo: CentrifugoService
}

const Chat = ({chatName}: Props) => {
    const [messages, setMessages] = useState<Array<MessageData>>([])
    const [currentMsg, setCurrentMsg] = useState<string>('')
    const [userId, setUserId] = useState<number | null>(null)
    const [currentPage, setCurrentPage] = useState<number | null>(1)
    const [openDialog, setOpenDialog] = useState<boolean>(false)
    const inputRef = useRef<HTMLDivElement>();
    const { centifugo } = useOutletContext<ContextType>()
    const navigate = useNavigate()
    const [blockChat, setBlockChat] = useState<boolean>(false)


    useEffect(() => {
        console.log('CHAT INITIAL USEEFFECT: ', chatName)
        console.log('CURRENT PAGE: ', currentPage)
        setCurrentPage(1)
        centifugo?.connectToChatRoom(chatName)
        centifugo?.getSubscription()?.on('publication', function(ctx) {
            console.log(ctx)
            if (ctx.data.status == 404) {
                setBlockChat(true)
                return
            }

            const message: MessageData = {
                message: ctx.data['message'],
                timestamp: ctx.data['timestamp'],
                user: Number(ctx.data['user'])
            }
            setMessages(prevMsg => [message, ...prevMsg])
        });
        const item = localStorage.getItem('userId')
        setUserId(item ? parseInt(JSON.parse(item)) : null)
        centifugo?.getHistory(chatName, 1)?.then(response => {
            setMessages(response.results.map((data: MessageResponse) => {
                return {
                    message: data.message,
                    timestamp: data.timestamp,
                    user: data.user
                }
            }))
        })
        return () => {
            console.log('CHAT DISMOUNT', chatName)
            centifugo?.disconnectFromChatRoom()
            setMessages([])
        }

    }, [chatName])


    const onScrollTop = useCallback(() => {
        if (!messages || !currentPage || blockChat)
            return
        const page = currentPage
        console.log('SCROLL TOP PAGE: ', page, 'CURRENT PAGE: ', currentPage)
        setCurrentPage(null)
        centifugo?.getHistory(chatName, page+1)?.then(response => {
            const newMessages = response.results.map((data: MessageResponse) => {
                return {
                    message: data.message,
                    timestamp: data.timestamp,
                    user: data.user
                }
            })
            console.log('NEW MESSAGES: ', newMessages)
            setMessages(messages => [...messages, ...newMessages])
            setCurrentPage((curr) => {
                return response.next ? page + 1 : null
        })
        })
    }, [currentPage, chatName])

    const handleSend = () => {
        const msg = currentMsg.trim()
        setCurrentMsg('')
        if (!msg) {
            return
        }
        centifugo?.publishMessage(msg)
        if (inputRef && inputRef.current)
            inputRef.current.focus()
    }

    const handleEnter = (event: any) => {
        if (event.key === 'Enter') {
            handleSend()
        }
    }

    const handleDeleteConveration = () => {
        centifugo?.removeConversation(chatName)
        setOpenDialog(false)
        navigate('/match')
    }

    return (
        <Box sx={{display: 'flex', flexDirection: 'column', height: '100%', minWidth: 260, width: '100%', bgcolor: '#FFFFFF', boxShadow: 1}}>
            <Box sx={{ height: '64px', minHeight: '64px'}}></Box>
            <MessageContainer messages={messages} userId={userId} onTopHandler={onScrollTop} />
            <Divider />
            <Container sx={{ display: 'flex', maxHeight: '45px', minHeight: '45px', width: '100%', alignItems: 'center'}}>
                <Button sx={{mr: '16px'}} onClick={() => setOpenDialog(true)} disabled={blockChat}>
                    <ClearIcon color='error' />
                </Button>
                <FormControl sx={{borderRadius: '20px', flexGrow: 1}}>
                    <TextField
                        hiddenLabel
                        size='small'
                        onChange={(e) => setCurrentMsg(e.target.value)}
                        value={(!blockChat) ? currentMsg : 'User refused further conversation.'}
                        sx={{borderRadius: '20px'}}
                        inputRef={inputRef}
                        onKeyDown={handleEnter}
                        autoFocus
                        disabled={blockChat}
                    />
                </FormControl>
                <Button sx={{ml: '16px'}} onClick={handleSend} disabled={blockChat}>
                    <SendIcon />
                </Button>
            </Container>
            <DeleteContactDialog open={openDialog} handleConfirm={handleDeleteConveration} handleClose={() => setOpenDialog(false)}/>
        </Box>
    );
}

export default Chat