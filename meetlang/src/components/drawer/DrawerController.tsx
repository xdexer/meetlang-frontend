import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import DrawerContent from './DrawerContent';
import { CONSTANTS } from '../../utils/consts';
import { useState, useEffect } from 'react';
import axios from 'axios'
import AuthService from '../../services/auth/auth-service';
import config from '../../production.config.json';
import jwtInterceptor from '../../utils/interceptors/jwtInterceptor';
import { ChatSummary } from '../../interfaces/chat';

interface Props {
    isOpen: boolean
    handleDrawerToggle: () => void
    handleDrawerOff: () => void
}



const DrawerController = (props: Props) => {
    const {isOpen, handleDrawerToggle, handleDrawerOff} = props
    const [chats, setChats] = useState<Array<ChatSummary>>([])

    useEffect(() => {
        getChats()
        const intervalId = setInterval(() => {
            getChats()
        }, 3500)

        return () => clearInterval(intervalId)
    }, [])

    const getChats = () => {
        jwtInterceptor.get(
            `${config.API_URL}/chat/chatroom/all_summary/`,
        ).then(response => {
            return setChats(response.data);
        })
    }

    return (
        <Box
            component="nav"
            sx={{ width: { sm: CONSTANTS.drawerWidth }, flexShrink: { sm: 0 } }}
            aria-label="mailbox folders"
        >
            {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
            <Drawer
                variant="temporary"
                open={isOpen}
                onClose={handleDrawerToggle}
                ModalProps={{
                    keepMounted: true, // Better open performance on mobile.
                }}
                sx={{
                    display: { xs: 'block', sm: 'none' },
                    '& .MuiDrawer-paper': { boxSizing: 'border-box', width: CONSTANTS.drawerWidth },
                }}
            >
                <DrawerContent chats={chats} handleDrawer={handleDrawerOff}/>
            </Drawer>
            <Drawer
                variant="permanent"
                sx={{
                    display: { xs: 'none', sm: 'block' },
                    '& .MuiDrawer-paper': { boxSizing: 'border-box', width: CONSTANTS.drawerWidth },
                }}
                open
            >
                <DrawerContent chats={chats} handleDrawer={handleDrawerOff}/>
            </Drawer>
        </Box>
    )
}

export default DrawerController