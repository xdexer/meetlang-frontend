import Divider from '@mui/material/Divider';
import List from '@mui/material/List';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import ContactRow from '../contacts/ContactRow';
import ListItem from '@mui/material/ListItem'
import ListItemText from '@mui/material/ListItemText'
import color from '../consts/colors';
import { ChatSummary } from '../../interfaces/chat';


interface Props {
    chats: Array<ChatSummary>,
    handleDrawer: () => void
}


const DrawerContent = (props: Props) => {

    return (
        <>
            <Toolbar />
            <Divider />
            <List sx={{ flexGrow: 1, overflowY: 'auto' }}>
                {(props.chats.length != 0) ? props.chats.map((chat) => (
                    <ContactRow 
                        key={chat.chat_id} 
                        chatData={chat}
                        handleDrawer={props.handleDrawer}
                    />
                )) :
                    <ListItem alignItems="flex-start" disablePadding>
                        <ListItemText
                        primary={'Let\'s Meet some langs!'}
                        sx={{paddingLeft: '10px'}}/>
                    </ListItem>
                }
            </List>
            <Divider />
            <Box sx={{ height: '126px', width: '100%', background: color.gradient }} />
        </>
    )
}

export default DrawerContent