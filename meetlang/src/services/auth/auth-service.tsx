import axios from 'axios'
import { bake_cookie, read_cookie} from 'sfcookies'
import config from '../../production.config.json'
import { LoginData, SignUpData } from './types'
import jwtInterceptor from '../../utils/interceptors/jwtInterceptor'

class AuthService {

    static signup = (signupData: SignUpData) => {
        return axios.post(
            `${config.API_URL}/auth/register/`,
            signupData
        ).then(response => {
            return response.data;
        })
    }

    static login = (loginData: LoginData) => {
        return axios.post(
            `${config.API_URL}/auth/login/`,
            loginData
        ).then(response => {
            console.log(response)
            if (response.data.access)
            {
                localStorage.setItem('user', JSON.stringify(response.data));
                bake_cookie('Authorization', `Bearer ${response.data.access}`)         
                console.log('LOGIN REFRESH TOKEN', read_cookie('refresh_token'))       
            }
    
            return response.data;
        })
    }

    static logout = () => {
        return jwtInterceptor.patch(
            `${config.API_URL}/user/user_management/${localStorage.getItem('userId')}/`,
            {
                is_active: false
            }
        ).then(response => {
            if (response) {
                console.log('LOGUT RESPONSE')
                localStorage.removeItem('userId')
                localStorage.removeItem('user');
                return response
            }
        })
    }

    static getUser = () => {
        if (localStorage.getItem('user'))
        {
            return JSON.parse(localStorage.getItem('user')!);
        }
        return null;
    }

    static getAuthHeader = () => {
        const user = this.getUser()
        if (user && user.access) {
            return { Authorization: `Bearer ${user.access}` }
        }
        return {}
    }

    static getToken = () => {
        const user = this.getUser()
        if (user && user.access) {
            return user.access
        }
        return {}
    }

    static isAuthenticated = (): boolean => {
        return localStorage.getItem('user') ? true : false
    }
}


export default AuthService;