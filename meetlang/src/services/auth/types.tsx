interface SignUpData {
    email: string
    password: string
    confirm_password: string
}

interface LoginData {
    email: string
    password: string
}


export type { SignUpData, LoginData }