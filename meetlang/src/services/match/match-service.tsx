import axios from 'axios'
import config from '../../production.config.json'
import AuthService from '../auth/auth-service'
import jwtInterceptor from '../../utils/interceptors/jwtInterceptor'


class MatchService {
    config: any
    
    constructor() {
        this.config = {
            headers: AuthService.getAuthHeader()
        }
    }
    acceptMatch = (id: number) => {
        return jwtInterceptor.post(
            `${config.API_URL}/user/match/accept/`,
            {
                'other_user': id
            },
            this.config
        ).then(response => {
            return response.data;
        })
    }

    refuseMatch = (id: number) => {
        return jwtInterceptor.post(
            `${config.API_URL}/user/match/refuse/`,
            {
                'other_user': id
            },
            this.config
        ).then(response => {
            return response.data
        })
    }

    getUserCardsInfo = () => {
        return jwtInterceptor.get(
            `${config.API_URL}/user/user_management/get_random_user/`,
            this.config
        ).then(response => {
            return response
        })
    }
}


export default MatchService;