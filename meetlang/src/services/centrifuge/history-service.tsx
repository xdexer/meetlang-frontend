import jwtInterceptor from "../../utils/interceptors/jwtInterceptor";
import config from '../../production.config.json'
import AuthService from "../auth/auth-service";


class ChatHistoryService {
    chatName: string = ''
    next: string | null = null
    previous: string | null = null

    getInitial(chatName: string) {
        this.chatName = chatName
        return jwtInterceptor.get(
            `${config.API_URL}/chat/message_history/`,
            {
                params: {
                    room: this.chatName,
                    page: 1
                }
            }
        ).then(response => {
            console.log('INITIAL')
            this.next = response.data.next;
            this.previous = response.data.next;
            return response.data
        })
    }

    getNextPage() {
        console.log('NEXT PAGE: ', this.next)
        if (!this.next)
            return

        return jwtInterceptor.get(
            this.next,
        ).then(response => {
            this.next = response.data.next;
            this.previous = response.data.previous;
            return response.data
        })
    }

    getPreviousPage() {
        if (!this.previous)
            return

        return jwtInterceptor.get(
            this.previous,
        ).then(response => {
            this.next = response.data.next;
            this.previous = response.data.previous;
            return response.data
        })
    }

}

export default ChatHistoryService;