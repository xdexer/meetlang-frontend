import { Centrifuge, Subscription, ConnectionTokenContext } from "centrifuge"
import jwtInterceptor from "../../utils/interceptors/jwtInterceptor";
import config from '../../production.config.json'
import AuthService from "../auth/auth-service";


class CentrifugoService {
    centrifuge: Centrifuge;
    config: any;
    subscription: Subscription | null = null;

    constructor(centrifuge: Centrifuge) {
        this.centrifuge = centrifuge;
        centrifuge.on('connecting', ctx => {
            console.log('CONNECTING: ', ctx)
        });
        this.centrifuge.on('connected', ctx => {
            console.log('CONNECTED: ', ctx)
        });
        this.centrifuge.on('error', ctx => {
            console.log('CENTRIFUGE ERROR', ctx)
        })
        this.centrifuge.on('disconnected', ctx => {
            console.log('DISCONECTED: ', ctx)
        });
        this.centrifuge.connect()
    }

    static getToken = (ctx: ConnectionTokenContext): Promise<string> => {
        return new Promise(resolve => {
            const token = JSON.parse(localStorage.getItem('user')!);
            console.log(ctx)
            if (token)
                resolve(
                    `Bearer ${token.access}`
                )
        })
    }

    connectToChatRoom(chatName: string | undefined) {
        if (!chatName)
            return
        console.log('CONNECT TO CHATROOM: ', chatName)
        this.subscription = this.centrifuge.newSubscription(chatName)
        this.subscription.subscribe()
    }

    getSubscription() {
        return this.subscription
    }

    getHistory(chatName: string | undefined, page: number | null = 1) {
        if (!this.subscription || !chatName || !page)
            return
        
        return jwtInterceptor.get(
            `${config.API_URL}/chat/message_history/`,
            {
                params: {
                    room: chatName,
                    page: page
                }
            }
        ).then(response => {
            console.log('history: ', response.data)
            return response.data
        })
    }

    disconnectFromChatRoom() {
        if (!this.subscription) 
            return;
        this.subscription.unsubscribe();
        this.subscription.removeAllListeners();
        this.centrifuge.removeSubscription(this.subscription);
        this.subscription = null;
    }

    publishMessage(msg: string) {
        this.subscription?.publish(
            {'input': msg}
        ).then(
            function() {},
            function(err) {
                console.log('PUBLISH ERROR: ', err)
            }
        )
    }

    disconnectClient() {
        this.centrifuge.disconnect()
    }

    removeConversation(chatName: string | undefined) {
        if (!this.subscription || !chatName)
            return

        return jwtInterceptor.delete(
            `${config.API_URL}/chat/chatroom/${chatName}`,
        )
    }

}

export default CentrifugoService