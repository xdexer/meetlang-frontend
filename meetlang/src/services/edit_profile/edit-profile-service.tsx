import config from '../../production.config.json'
import AuthService from '../auth/auth-service'
import jwtInterceptor from '../../utils/interceptors/jwtInterceptor'
import { ProfileRequest } from '../../interfaces/edit-profile'

class EditProfileService {

    static getUserDefaults() {
        const user = localStorage.getItem('userId')
        return jwtInterceptor.get(
            `${config.API_URL}/user/user_management/${user}/`,
        ).then(response => {
                return response
        })
    }

    static getLanguages() {
        return jwtInterceptor.get(
            `${config.API_URL}/user/language/`,
        ).then(response => {
            return response
        })
    }

    static getTags() {
        return jwtInterceptor.get(
            `${config.API_URL}/user/personality_tag/`,
        ).then(response => {
            return response
        })
    }

    static postData(data: ProfileRequest) {
        const isFirstLogin = localStorage.getItem('firstLogin')
        if (isFirstLogin === "true")
            return jwtInterceptor.post(
                `${config.API_URL}/user/user_management/`,
                {
                    ...this.prepareRequest(data),
                    user: localStorage.getItem('userId')
                },
            ).then(response => {
                if (response) {
                    localStorage.removeItem('firstLogin')
                    return response
                }
            })
        return jwtInterceptor.patch(
            `${config.API_URL}/user/user_management/${localStorage.getItem('userId')}/`,
            this.prepareRequest(data),
        )
    }

    private static prepareRequest(data: ProfileRequest) {
        return {
            first_name: data.name,
            user_description: data.description,
            age: data.age,
            sex: Boolean(data.age),
            native_language: data.nativeLanguage,
            language_to_learn: data.desiredLanguage,
            tags: data.tags.map(el => el.id),
        }
    }

    static uploadImageRequest = (data: ProfileRequest) => {
        return jwtInterceptor.patch(
            `${config.API_URL}/user/user_management/${localStorage.getItem('userId')}/`,
            this.prepareImage(data),
        )
    }

    private static prepareImage(data: ProfileRequest) {
        const formData = new FormData()
        formData.append('avatar', data.avatar as Blob)
        return formData
    }
}

export default EditProfileService;