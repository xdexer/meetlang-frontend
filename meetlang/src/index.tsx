import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import reportWebVitals from './reportWebVitals';
import {
  RouterProvider,
  createBrowserRouter,
} from "react-router-dom";
import LoginPage from './routes/auth/LoginPage';
import SignUpPage from './routes/auth/SignUpPage';
import MainPage from './routes/main/MainPage';
import RouteGuard from './routes/RouteGuard';
import ChatPage from './routes/main/ChatPage';
import UserCardPage from './routes/main/UserCardPage';
import MatchService from './services/match/match-service';
import EditProfilePage from './routes/main/EditProfilePage';

const router = createBrowserRouter([
  {
    element: <RouteGuard route={<MainPage />} />,
    path: '/',
    children: [
      {
        path: 'chat/:chatName',
        element: <ChatPage />
      },
      {
        path: 'match/',
        element: <UserCardPage />
      },
      {
        path: 'edit_profile/',
        element: <EditProfilePage />
      }
    ]
  },
  {
    element: <LoginPage />,
    path: '/login',
  },
  {
    element: <SignUpPage />,
    path: '/signup',
  },
])

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <RouterProvider router={router} />
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
