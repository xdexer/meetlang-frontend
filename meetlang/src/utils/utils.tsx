import { DAY_MAPPING, ONE_DAY } from './consts'

const formatTimestamp = (timestamp: string): string => {
    const date = new Date(timestamp)
    const now = new Date(Date.now())
    const difference = now.getTime() - date.getTime()
    const timeString = date.toLocaleTimeString(['pl'], { hour: "2-digit", minute: "2-digit" })
    const dateString = date.toLocaleDateString(['pl'])

    if (date.toDateString() !== now.toDateString()) {
        if ((difference/ONE_DAY) > 7)
            return `${dateString}, ${timeString} `
        return `${DAY_MAPPING.get(date.getUTCDay())}, ${timeString} `
    }
    return `${timeString}`
}

const stringToColor = (string: string): string => {
    let hash = 0;
    let i;
  
    /* eslint-disable no-bitwise */
    for (i = 0; i < string.length; i += 1) {
      hash = string.charCodeAt(i) + ((hash << 5) - hash);
    }
  
    let color = '#';
  
    for (i = 0; i < 3; i += 1) {
      const value = (hash >> (i * 8)) & 0xff;
      color += `00${value.toString(16)}`.slice(-2);
    }
    /* eslint-enable no-bitwise */
  
    return color;
}


const stringAvatar = (name: string, options:any = {}) => {
  const newName = name.trim().split(' ')
  let avatarName = (newName.length > 1) ? `${newName[0][0]}${newName[1][0]}` : `${newName[0]}`

  return {
    sx: {
      bgcolor: stringToColor(name),
      ...options
    },
    children: avatarName,
  };
}


export { stringAvatar, stringToColor, formatTimestamp }
