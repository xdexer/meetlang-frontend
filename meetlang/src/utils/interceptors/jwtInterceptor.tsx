import axios, { AxiosRequestConfig } from "axios";
import { bake_cookie  } from "sfcookies";
import config from "../../production.config.json"
import AuthService from "../../services/auth/auth-service";

const jwtInterceptor = axios.create({});

jwtInterceptor.interceptors.request.use((config: any) => {
  let user = JSON.parse(localStorage.getItem("user") as string);
  config.headers.Authorization = `Bearer ${user.access}`;
  return config;
});

jwtInterceptor.interceptors.response.use(
  (response) => {
    return response;
  },
  async (error) => {
    if (error.response.status === 401) {
      const authData = JSON.parse(localStorage.getItem("user") as string);
      console.log('INVALID TOKEN', authData)
      let response = await axios.post(
        `${config.API_URL}/auth/login/refresh/`
      );
      console.log('INTERCEPTOR RESPONES DATA: ', response.data)
      localStorage.setItem("user", JSON.stringify(response.data));
      bake_cookie('Authorization', `Bearer ${response.data.access}`)         
      console.log('TOKEN FROM AUTHSERVICE: ', AuthService.getToken())
      error.config.headers[
        "Authorization"
      ] = `Bearer ${response.data.access}`;
      return axios(error.config);
    } else {
      return Promise.reject(error);
    }
  }
);
export default jwtInterceptor;