const CONSTANTS = {
    drawerWidth: 240
}

const DAY_MAPPING = new Map<number, string>([
    [0, 'SUN'],
    [1, 'MON'],
    [2, 'TUE'],
    [3, 'WED'],
    [4, 'THR'],
    [5, 'FRI'],
    [6, 'SAT']
])

const ONE_DAY = 1000*60*60*24;
const ONE_HOUR = 1000*60*60

export { DAY_MAPPING, ONE_DAY, ONE_HOUR, CONSTANTS}
